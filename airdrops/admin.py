from django import forms
from django.contrib import admin
from ckeditor.widgets import CKEditorWidget

from airdrops.models import Airdrop, Like, Coin


class AirdropLikesAdmin(admin.TabularInline):
    model = Like


class AirdropAdminForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Airdrop
        fields = "__all__"


@admin.register(Coin)
class CoinAdmin(admin.ModelAdmin):
    model = Coin
    list_display = ["name", "short_name", "price", "cmc_rank", "max_supply", "circulating_supply", "market_cap"]
    ordering = ["name"]
    search_fields = ["name", "short_name"]

    fieldsets = (
        (None, {
            "fields": (
                "name", "short_name", "price", "cmc_rank", "max_supply", "circulating_supply", "market_cap",
                "logo_from_api"
            ),
        }),
    )


@admin.register(Airdrop)
class AirdropAdmin(admin.ModelAdmin):
    list_display = [
        "name", "status", "coin", "platform", "difficulty", "end_datetime", "exclusive", "value", "manual_value", "value_type",
        "token", "referral", "manual_referral", "created_at", "get_likes",
    ]
    search_fields = ["name"]
    fieldsets = (
        (None, {
            "fields": (
                "name", "description", "difficulty", "status", "content", "coin", "logo", "platform", "end_datetime",
                "exclusive", "value", "manual_value", "value_type", "referral", "manual_referral", "token",
            ),
        }),
    )
    inlines = [AirdropLikesAdmin]
    form = AirdropAdminForm

    def get_likes(self, obj):
        return obj.get_likes_count()

    get_likes.short_description = "Count of likes"
