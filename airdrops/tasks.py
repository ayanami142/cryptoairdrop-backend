from __future__ import absolute_import, unicode_literals
from celery import shared_task
import logging

from pycoingecko import CoinGeckoAPI

from airdrops.models import TopCoin, Coin

logger = logging.getLogger(__name__)


@shared_task
def update_top_coins_list():
    cg = CoinGeckoAPI()

    if cg.ping().get('gecko_says') == '(V3) To the Moon!':
        cg_response = cg.get_coins_markets(vs_currency="usd", order="gecko_desc", per_page=10,
                                           price_change_percentage="1h")

        TopCoin.objects.all().delete()

        for cg_coin in cg_response:
            TopCoin.objects.create(
                name=cg_coin.get('symbol', None),
                price=cg_coin.get('current_price', None),
                is_increased=True if cg_coin.get('price_change_percentage_1h_in_currency') >= 0 else False
            )


@shared_task
def update_coin_list():
    cg = CoinGeckoAPI()
    coins_list = Coin.objects.all().values_list('short_name', flat=True)
    page = 1
    while True:
        coin_data_response = cg.get_coins_markets(vs_currency='usd', per_page='250', page=page)
        if not coin_data_response:
            break
        for coin_data in coin_data_response:
            if coin_data.get('symbol') in coins_list:
                Coin.objects.filter(short_name=coin_data.get('symbol')).update(
                    name=coin_data.get('name'),
                    short_name=coin_data.get('symbol'),
                    price=coin_data.get('current_price'),
                    cmc_rank=coin_data.get('market_cap_rank'),
                    max_supply=coin_data.get('max_supply'),
                    circulating_supply=coin_data.get('circulating_supply'),
                    market_cap=coin_data.get('market_cap'),
                    logo_from_api=coin_data.get('image')
                )
            else:
                Coin.objects.create(
                    name=coin_data.get('name'),
                    short_name=coin_data.get('symbol'),
                    price=coin_data.get('current_price'),
                    cmc_rank=coin_data.get('market_cap_rank'),
                    max_supply=coin_data.get('max_supply'),
                    circulating_supply=coin_data.get('circulating_supply'),
                    market_cap=coin_data.get('market_cap'),
                    logo_from_api=coin_data.get('image')
                )
        page += 1

    for row in Coin.objects.all().reverse():
        if Coin.objects.filter(name=row.name).count() > 1:
            row.delete()
