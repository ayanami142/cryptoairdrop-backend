from datetime import datetime
from django.db import IntegrityError
from django.db.models import Q
from rest_framework import viewsets
from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from airdrops.models import Airdrop, Like, TopCoin
from airdrops.serializers import AirdropRetrieveSerializer, AirdropListSerializer, TopCoinListSerializer


class AirdropViewSet(viewsets.ModelViewSet):
    serializer_class = AirdropListSerializer
    queryset = Airdrop.objects.all().order_by('-created_at')
    http_method_names = ('get',)

    def get_queryset(self):
        if self.action == 'retrieve':
            return self.queryset

        filter_type = self.request.query_params.get('type')
        name = self.request.query_params.get('name')
        if not filter_type and not name:
            return self.queryset.exclude(end_datetime__lte=datetime.now())

        if filter_type == 'popular':
            likes_list_id = Like.objects.all().values_list('id', flat=True)
            self.queryset = self.queryset.filter(likes__id__in=likes_list_id).distinct()
        elif filter_type == 'ended':
            ended_airdrops = Airdrop.objects.filter(end_datetime__lte=datetime.now())
            for ended_airdrop in ended_airdrops:
                ended_airdrop.status = Airdrop.Status.ENDED
                ended_airdrop.save()
            self.queryset = self.queryset.filter(end_datetime__lte=datetime.now())
        elif filter_type == 'liked':
            if self.request.user.is_authenticated:
                likes_list_id = Like.objects.filter(user=self.request.user).values_list('id', flat=True)
                print(likes_list_id)
                self.queryset = self.queryset.filter(likes__id__in=likes_list_id)
        if name and filter_type != 'ended':
            self.queryset = self.queryset.filter(
                Q(name__icontains=name) & (Q(end_datetime__gte=datetime.now()) | Q(end_datetime=None)))
        if name and filter_type == 'ended':
            self.queryset = self.queryset.filter(name__icontains=name)
        return self.queryset

    def get_permissions(self):
        if self.action == 'retrieve':
            return [IsAuthenticated(), ]
        elif self.action == 'list':
            return [AllowAny(), ]

    action_serializers = {
        'retrieve': AirdropRetrieveSerializer,
        'list': AirdropListSerializer,
    }

    def get_serializer_class(self):
        if hasattr(self, 'action_serializers'):
            return self.action_serializers.get(self.action, self.serializer_class)
        return super(AirdropViewSet, self).get_serializer_class()


class AirdropsCount(APIView):
    permission_classes = [AllowAny, ]

    def get(self, request):
        likes_list_id = Like.objects.all().values_list('id', flat=True)
        popular_count = Airdrop.objects.filter(likes__id__in=likes_list_id).distinct().count()

        if self.request.user.is_authenticated:
            likes_list_id = Like.objects.filter(user=self.request.user).values_list('id', flat=True)
            print(likes_list_id)
            likes_count = Airdrop.objects.filter(likes__id__in=likes_list_id).count()
        else:
            likes_count = 0

        response = {
            'active': Airdrop.objects.exclude(
                Q(status='E')).filter((Q(end_datetime__gte=datetime.now()) | Q(end_datetime=None))).count(),
            'popular': popular_count,
            'ended': Airdrop.objects.filter(end_datetime__lte=datetime.now()).count(),
            'liked': likes_count
        }

        return Response(response, status=200)


class GetExclusiveAirdrops(APIView):
    authentication_classes = []
    permission_classes = [AllowAny, ]

    def get(self, request):
        return Response(
            AirdropListSerializer(Airdrop.objects.filter(
                Q(exclusive=True) & (Q(end_datetime__gte=datetime.now()) | Q(end_datetime=None))).order_by(
                '-created_at'), many=True).data,
            status=200)


class LikeAirdrop(APIView):
    permission_classes = [IsAuthenticated, ]

    def post(self, request, id):
        try:
            Like.objects.create(
                user=request.user,
                airdrop_id=id
            )
        except IntegrityError:
            return Response({"error": str("You already like this airdrop")}, status=400)

        return Response(status=201)


class UnlikeAirdrop(APIView):
    permission_classes = [IsAuthenticated, ]

    def post(self, request, id):
        Like.objects.filter(
            user=request.user,
            airdrop_id=id
        ).delete()

        return Response(status=204)


class TopCoinViewSet(ListAPIView):
    permission_classes = [AllowAny, ]
    serializer_class = TopCoinListSerializer
    queryset = TopCoin.objects.all()
    http_method_names = ('get',)
