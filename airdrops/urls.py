from django.conf.urls.static import static
from django.urls import path, include
from rest_framework import routers
from django.conf import settings

from airdrops.views import AirdropViewSet, AirdropsCount, GetExclusiveAirdrops, LikeAirdrop, UnlikeAirdrop, \
    TopCoinViewSet

router = routers.SimpleRouter()
router.register(r'airdrops', AirdropViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path("airdrops/airdrops-count", AirdropsCount.as_view(), name="airdrops"),
    path("airdrops/get-currencies", TopCoinViewSet.as_view(), name="get-currencies"),
    path("airdrops/get-exclusive-airdrops", GetExclusiveAirdrops.as_view(), name="get-exclusive-airdrops"),
    path("airdrops/like/<int:id>", LikeAirdrop.as_view(), name="like"),
    path("airdrops/unlike/<int:id>", UnlikeAirdrop.as_view(), name="unlike"),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

