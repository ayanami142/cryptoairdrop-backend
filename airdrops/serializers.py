from rest_framework import serializers

from airdrops.models import Airdrop, TopCoin, Coin


class CoinSerializer(serializers.ModelSerializer):
    class Meta:
        model = Coin
        fields = (
            'name',
            'short_name',
            'price',
            'cmc_rank',
            'max_supply',
            'circulating_supply',
            'market_cap',
            'logo_from_api'
        )


class AirdropListSerializer(serializers.ModelSerializer):
    is_liked = serializers.SerializerMethodField(read_only=True)
    coin = CoinSerializer()

    class Meta:
        model = Airdrop
        fields = (
            'id',
            'name',
            'status',
            'end_datetime',
            'exclusive',
            'coin',
            'value',
            'manual_value',
            'value_type',
            'difficulty',
            'logo',
            'is_liked',
        )

    def get_is_liked(self, obj: Airdrop) -> bool:
        request = self.context.get('request')
        if hasattr(request, 'user'):
            user = request.user
            if user.is_anonymous:
                return False
        else:
            return False

        return True if obj.likes.filter(user=user).exists() else False


class AirdropRetrieveSerializer(serializers.ModelSerializer):
    quantity_of_likes = serializers.ReadOnlyField(source='get_likes_count')
    coin = CoinSerializer()

    class Meta:
        model = Airdrop
        fields = (
            'id',
            'name',
            'description',
            'status',
            'end_datetime',
            'exclusive',
            'coin',
            'platform',
            'value',
            'manual_value',
            'value_type',
            'token',
            'referral',
            'manual_referral',
            'content',
            'logo',
            'quantity_of_likes',
        )


class TopCoinListSerializer(serializers.ModelSerializer):
    class Meta:
        model = TopCoin
        fields = (
            'name',
            'price',
            'is_increased',
        )
