from django.apps import AppConfig


class AirdropsConfig(AppConfig):
    name = 'airdrops'
