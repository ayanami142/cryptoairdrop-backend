from django.db import models
from django.utils.translation import gettext as _
from users.models import User


class Coin(models.Model):
    name = models.CharField(unique=True, max_length=100, null=True, blank=True)
    short_name = models.CharField(max_length=50, null=True, blank=True)
    price = models.FloatField(null=True, blank=True)
    cmc_rank = models.CharField(max_length=50, blank=True, null=True)
    max_supply = models.CharField(max_length=50, blank=True, null=True)
    circulating_supply = models.CharField(max_length=50, blank=True, null=True)
    market_cap = models.CharField(max_length=50, blank=True, null=True)
    logo_from_api = models.URLField(blank=True, null=True, max_length=500)

    class Meta:
        verbose_name = _("Coin")
        verbose_name_plural = _("Coins")

    def __str__(self):
        return "{} - {}".format(self.short_name, self.name)


class Airdrop(models.Model):
    class Status(models.TextChoices):
        ACTIVE = 'A', _("active")
        NEW = 'N', _("new")
        ENDED = 'E', _("ended")

    class ValueType(models.TextChoices):
        VALUE = 'V', _("value")
        FROM = 'F', _("from")
        SHARE_OF = 'SO', _("share_of")
        UP_TO = 'UT', _("up_to")
        AIRDROP_POOL = 'AP', _("airdrop_pool")
        FOR_HOLDERS = 'FH', _("for_holders")

    class Difficulty(models.TextChoices):
        EASY = 'E', _("easy")
        MEDIUM = 'M', _("medium")
        HARD = 'H', _("hard")

    name = models.CharField(max_length=256)
    description = models.TextField(null=True)
    status = models.CharField(max_length=1, choices=Status.choices, default=Status.NEW)
    difficulty = models.CharField(max_length=1, choices=Difficulty.choices, default=Difficulty.EASY)
    end_datetime = models.DateTimeField(null=True, blank=True)
    exclusive = models.BooleanField(default=False)
    platform = models.CharField(max_length=100)
    value = models.FloatField(null=True, blank=True)
    manual_value = models.FloatField(null=True, blank=True,
                                     help_text="Change if you want to set manual value instead of multiply count of "
                                               "token and price")
    logo = models.ImageField(blank=True, null=True, max_length=500)
    value_type = models.CharField(max_length=2, choices=ValueType.choices, default=ValueType.VALUE)
    token = models.FloatField()
    referral = models.FloatField(help_text="Set 0 if you want to use manual referral field")
    manual_referral = models.FloatField(max_length=50, null=True, blank=True,
                                       help_text="Change if you want to set manual referral value instead of multiply "
                                                 "count of referral and price")
    content = models.TextField(null=True, blank=True, verbose_name=_("Airdrop content"))
    coin = models.ForeignKey(Coin, on_delete=models.CASCADE, related_name='coin')
    created_at = models.DateTimeField(auto_now_add=True)

    def get_likes_count(self):
        return self.likes.count()

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.coin.price:
            self.coin.price = 1.0
        if not self.manual_value:
            self.value = round(self.token * self.coin.price, 2)
        if not self.manual_referral:
            self.referral = round(self.referral * self.coin.price, 2)
        if self.manual_referral:
            self.manual_referral = round(self.manual_referral * self.coin.price, 2)
        super(Airdrop, self).save(*args, **kwargs)


class Like(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="likes")
    airdrop = models.ForeignKey(Airdrop, on_delete=models.CASCADE, related_name="likes")

    class Meta:
        unique_together = ['user', 'airdrop']
        verbose_name = _("Like")
        verbose_name_plural = _("Likes")


class TopCoin(models.Model):
    name = models.CharField(max_length=50, null=True, blank=True)
    price = models.FloatField(null=True, blank=True)
    is_increased = models.BooleanField(null=True, blank=True)

class hello(int):
