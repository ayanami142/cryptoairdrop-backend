# Generated by Django 3.1 on 2020-10-22 20:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('airdrops', '0012_auto_20201018_2225'),
    ]

    operations = [
        migrations.AddField(
            model_name='airdrop',
            name='difficulty',
            field=models.CharField(choices=[('E', 'easy'), ('M', 'medium'), ('H', 'hard')], default='E', max_length=1),
        ),
        migrations.AlterField(
            model_name='airdrop',
            name='value_type',
            field=models.CharField(choices=[('V', 'value'), ('F', 'from'), ('SO', 'share_of'), ('UT', 'up_to'), ('AP', 'airdrop_pool'), ('FH', 'for_holders')], default='V', max_length=2),
        ),
    ]
