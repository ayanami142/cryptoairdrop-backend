from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.text import gettext_lazy as _
from users.managers import CustomUserManager


class User(AbstractUser):
    username = None
    first_name = models.CharField(_('first name'), max_length=30, null=True, blank=True)
    email = models.EmailField(_('Email address'), unique=True)
    is_active = models.BooleanField(
        _('active'),
        default=False,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )
    photo = models.URLField(_('Social Thumb'), blank=True, null=True, max_length=500)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
    objects = CustomUserManager()

    def activate_user(self):
        self.is_active = True
        self.save()

    def __str__(self):
        return self.email
