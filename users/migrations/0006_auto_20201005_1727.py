# Generated by Django 3.1 on 2020-10-05 17:27

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0005_remove_user_photo'),
    ]

    operations = [
        migrations.RenameField(
            model_name='user',
            old_name='social_thumb',
            new_name='photo',
        ),
    ]
