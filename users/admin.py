from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.utils.text import gettext_lazy as _

from users.models import User


class UserAdmin(BaseUserAdmin):
    list_display = ('email', 'first_name', 'last_name', 'date_joined', 'is_active', 'is_superuser')

    fieldsets = (
        (None, {'fields': (
            'email', 'password', 'is_active', 'date_joined', 'first_name', 'last_name', 'photo'),
        }),
        (_('Permissions'), {
            'fields': ('is_superuser', 'is_staff', 'groups', 'user_permissions'),
        }),
    )

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2'),
        }),
    )

    search_fields = ('email',)
    ordering = ('email',)


admin.site.register(User, UserAdmin)
