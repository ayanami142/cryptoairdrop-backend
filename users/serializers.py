from djoser.serializers import UserCreateSerializer
from rest_framework import serializers
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from django.core.validators import validate_email


class UserCreateProfileSerializer(UserCreateSerializer):
    id = serializers.IntegerField(read_only=True)
    first_name = serializers.CharField(required=True)
    date_joined = serializers.DateTimeField(read_only=True)

    class Meta(UserCreateSerializer.Meta):
        fields = (
            'id',
            'first_name',
            'last_name',
            'email',
            'password',
            'date_joined',
        )


class UserGetProfileSerializer(UserCreateProfileSerializer):
    class Meta(UserCreateProfileSerializer.Meta):
        fields = UserCreateProfileSerializer.Meta.fields + (
            'photo',
        )


class TokenObtainPairCustomSerializer(TokenObtainPairSerializer):
    def validate(self, attrs):
        try:
            validate_email(attrs.get('email'))
        except Exception:
            raise serializers.ValidationError(
                {"email": "Please enter a valid email address"}
            )
        return super().validate(attrs)
