from django.conf.urls import url
from django.contrib.auth import get_user_model
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from django.contrib import admin
from rest_framework import permissions
from django.urls import path, include
from rest_framework import routers
from django.conf.urls.static import static
from django.conf import settings
from users.views import TokenObtainPairCustomView, UserNestedViewSet

User = get_user_model()

schema_view = get_schema_view(
    openapi.Info(
        title="Cryptoairdrop API",
        default_version='v1'
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

router = routers.DefaultRouter()
router.register("auth/users", UserNestedViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('jet/', include('jet.urls', 'jet')),
    path('jet/dashboard/', include('jet.dashboard.urls', 'jet-dashboard')),
    url(r'^', include('airdrops.urls')),
    path('admin/', admin.site.urls),
    url(r"^auth/jwt/create/?", TokenObtainPairCustomView.as_view(), name="jwt-create"),
    url(r'^auth/', include('djoser.urls.jwt')),
    url(r'^api/login/', include('rest_social_auth.urls_jwt_pair')),
    url(r'^swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
