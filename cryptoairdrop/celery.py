from __future__ import absolute_import, unicode_literals

import os

from celery import Celery

# set the default Django settings module for the 'celery' program.
from celery.schedules import crontab

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'cryptoairdrop.settings')

app = Celery('cryptoairdrop')

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings', namespace='CELERY')

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()

app.conf.beat_schedule = {
    'update-currencies-every-1-hour': {
        'task': 'airdrops.tasks.update_top_coins_list',
        'schedule': crontab(minute=0, hour='*/1')
    },
    'update_coin_list-every-day': {
        'task': 'airdrops.tasks.update_coin_list',
        'schedule': crontab(minute=0, hour=0)
    }
}


@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))
