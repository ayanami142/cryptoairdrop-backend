FROM python:3.8-slim-buster

ENV PYTHONUNBEFFERED 1

COPY requirements.txt /requirements.txt
RUN apt-get update
RUN apt-get install -y libpq-dev gcc

RUN pip install -r requirements.txt

RUN mkdir /app
WORKDIR /app
COPY . /app

CMD ["python", "/app/manage.py", "runserver", "127.0.0.1:8000"]